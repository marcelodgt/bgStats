const xml2js = require('xml2js');
const request = require('request');

const parser = xml2js.Parser();

const getTotalPlays = username => new Promise((resolve, reject) => {
  request(`https://www.boardgamegeek.com/xmlapi2/plays?username=${username}`, (error, response, html) => {
    if (!error || response.statusCode === 200) {
      console.log(response);
      parser.parseString(html, (err, result) => {
        const totalPlays = result.plays.$.total;
        // console.log(result);
        resolve(totalPlays);
      });
    } else {
      reject(console.log('Conection error'));
    }
  });
});

const getPlaysData = username => new Promise((resolve, reject) => {
  getTotalPlays(username).then((res) => {
    const plays = [];
    const totalPlays = res;
    let playNumber = 1;
    let totalPages = 0;
    if (totalPlays % 100 === 0) {
      totalPages = Math.floor(totalPlays / 100);
    } else {
      totalPages = Math.floor((totalPlays / 100) + 1);
    }
    console.log(totalPages);
    for (let indexPage = 1; indexPage <= totalPages; indexPage += 1) {
      // let numGame = 0;
      request(`https://www.boardgamegeek.com//xmlapi2/plays?subtype=boardgame&username=${username}&page=${indexPage}`, (error, response, html) => {
        if (!error || response.statusCode === 200) {
          parser.parseString(html, (err, result) => {
            const playsPerPage = Object.keys(result.plays.play).length;
            // console.log(Object.keys(result.plays).length);
            for (let indexGame = 0; indexGame < playsPerPage; indexGame += 1) {
              const players = [];
              const playDate = result.plays.play[indexGame].$.date;
              const playLocation = result.plays.play[indexGame].$.location;
              const playGame = result.plays.play[indexGame].item[0].$.name;
              const playQtyPlayers = Object.keys(result.plays.play[indexGame].players[0].player).length;
              for (let j = 0; j < playQtyPlayers; j += 1) {
                const playerName = result.plays.play[indexGame].players[0].player[j].$.name;
                const playerBggId = result.plays.play[indexGame].players[0].player[j].$.username;
                const playerRole = result.plays.play[indexGame].players[0].player[j].$.color;
                const playerScore = result.plays.play[indexGame].players[0].player[j].$.score;
                const playerWin = result.plays.play[indexGame].players[0].player[j].$.win;
                const playerInfo = {
                  playerName,
                  playerBggId,
                  playerRole,
                  playerScore,
                  playerWin,
                };
                players.push(playerInfo);
              }
              const playInfo = {
                playNumber,
                playDate,
                playLocation,
                playGame,
                playQtyPlayers,
                players,
              };
              plays.push(playInfo);
              playNumber += 1;
            }
          });
        } else {
          reject(console.log('Something went wrong with the connection'));
        }
      });
    }
    resolve(plays);
  }, (errorMessage) => {
    console.log(errorMessage);
  });
});

const username = 'marcelodgt';

getPlaysData(username).then((res) => {
  console.log(res);
}, (erroMessage) => {
  console.log(erroMessage);
});
