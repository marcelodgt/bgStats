const xml2js = require('xml2js');
const request = require('async-request');
const Promise = require('bluebird');

const parser = new xml2js.Parser();

const bggApiUrl = 'https://www.boardgamegeek.com/xmlapi2/';

async function getTotalPlays(username) {
  let totalPlays = 0;
  let bggPlaysData = '';
  try {
    bggPlaysData = await request(`${bggApiUrl}plays?username=${username}`);
    const playsData = bggPlaysData.body;
    await parser.parseString(playsData, (err, result) => {
      totalPlays = result.plays.$.total;
    });
    return totalPlays;
  } catch (err) {
    return err;
  }
}

function getPlayersInfo(totalPlayers, data) {
  const players = [];
  for (let j = 0; j < totalPlayers; j += 1) {
    const playerName = data[j].$.name;
    const playerBggId = data[j].$.username;
    const playerRole = data[j].$.color;
    const playerScore = data[j].$.score;
    const playerWin = data[j].$.win;
    const playerInfo = {
      playerName,
      playerBggId,
      playerRole,
      playerScore,
      playerWin,
    };
    players.push(playerInfo);
  }
  return players;
}

async function getBggPlaysData(username) {
  const plays = [];
  let playNumber = 1;
  let totalPages = 0;

  try {
    const totalPlays = await getTotalPlays(username);

    if (totalPlays % 100 === 0) {
      totalPages = Math.floor(totalPlays / 100);
    } else {
      totalPages = Math.floor((totalPlays / 100) + 1);
    }

    const pagesNumbers = [];
    for (let i = 1; i <= totalPages; i += 1) {
      pagesNumbers.push(i);
    }

    await Promise.mapSeries(pagesNumbers, async (pageNumber) => {
      const bggPlaysData = await request(`${bggApiUrl}plays?username=${username}&page=${pageNumber}`);
      const playsData = bggPlaysData.body;
      await parser.parseString(playsData, (err, result) => {
        const playsPerPage = Object.keys(result.plays.play).length;
        for (let indexGame = 0; indexGame < playsPerPage; indexGame += 1) {
          const playDate = result.plays.play[indexGame].$.date;
          const playLocation = result.plays.play[indexGame].$.location;
          const playGame = result.plays.play[indexGame].item[0].$.name;
          const playQtyPlayers = Object.keys(result.plays.play[indexGame].players[0].player).length;
          const players = result.plays.play[indexGame].players[0].player;
          getPlayersInfo(playQtyPlayers, players);
          const playInfo = {
            playNumber,
            playDate,
            playLocation,
            playGame,
            playQtyPlayers,
            players,
          };
          plays.push(playInfo);
          playNumber += 1;
        }
      });
    });

    return plays;
  } catch (e) {
    throw e;
  }
}

async function getPlaysData(username) {
  const bggPlaysData = await getBggPlaysData(username);
  // console.log(bggPlaysData[0].playGame);
  // console.log(bggPlaysData[0].players[0].$.name);
  // console.log(bggPlaysData[0].players[0]);
  // console.log(bggPlaysData);
  return bggPlaysData;
}
getPlaysData('marcelodgt');
