const xml2js = require('xml2js');
const request = require('async-request');

const parser = new xml2js.Parser();

const bggApiUrl = 'https://www.boardgamegeek.com/xmlapi2/';

function sleep(ms = 0) {
  return new Promise(r => setTimeout(r, ms));
}

async function getTotalPlays(username) {
  let totalPlays = 0;
  let bggPlaysData = '';
  try {
    bggPlaysData = await request(`${bggApiUrl}plays?username=${username}`);
    const playsData = bggPlaysData.body;
    await parser.parseString(playsData, (err, result) => {
      totalPlays = result.plays.$.total;
    });
    return totalPlays;
  } catch (err) {
    return err;
  }
}

async function getBggPlaysData(username) {
  const plays = [];
  let playNumber = 1;
  let playersData = [];
  let totalPages = 0;

  try {
    const totalPlays = await getTotalPlays(username);

    if (totalPlays % 100 === 0) {
      totalPages = Math.floor(totalPlays / 100);
    } else {
      totalPages = Math.floor((totalPlays / 100) + 1);
    }

    for (let indexPage = 1; indexPage <= totalPages; indexPage += 1) {
      bggPlaysData = await request(`${bggApiUrl}plays?username=${username}&page=${indexPage}`);
      const playsData = bggPlaysData.body;
      await parser.parseString(playsData, (err, result) => {
        const playsPerPage = Object.keys(result.plays.play).length;
        console.log(playsPerPage);
        for (let indexGame = 0; indexGame < playsPerPage; indexGame += 1) {
          const playDate = result.plays.play[indexGame].$.date;
          const playLocation = result.plays.play[indexGame].$.location;
          const playGame = result.plays.play[indexGame].item[0].$.name;
          const playQtyPlayers = Object.keys(result.plays.play[indexGame].players[0].player).length;
          const players = result.plays.play[indexGame].players[0].player;
          playersData = getPlayersInfo(playQtyPlayers, players);
          const playInfo = {
            playNumber,
            playDate,
            playLocation,
            playGame,
            playQtyPlayers,
            players,
          };
          plays.push(playInfo);
          playNumber += 1;
        }
      });
    }
    return plays;
  } catch (e) {
    console.log(e);
  }
}

function getPlayersInfo(totalPlayers, data) {
  const players = [];
  for (let j = 0; j < totalPlayers; j += 1) {
    const playerName = data[j].$.name;
    const playerBggId = data[j].$.username;
    const playerRole = data[j].$.color;
    const playerScore = data[j].$.score;
    const playerWin = data[j].$.win;
    const playerInfo = {
      playerName,
      playerBggId,
      playerRole,
      playerScore,
      playerWin,
    };
    players.push(playerInfo);
  }
  return players;
}

async function funcaoDois() {
  await sleep(250);
  return 'Dois';
}

async function getPlaysInfo(username) {
  const bggPlaysData = await getBggPlaysData(username);
  const dois = await funcaoDois();
  console.log(bggPlaysData);
  console.log(dois);
}


getPlaysInfo('marcelodgt');
