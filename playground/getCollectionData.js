const xml2js = require('xml2js');
const request = require('request');

const parser = xml2js.Parser();

const username = 'marcelodgt';
const bgArray = [];
const bgExpansionsArray = {};

const getCollectionData = (username) => {
  return new Promise((reject, resolve) => {
    request(`https://www.boardgamegeek.com/xmlapi2/collection?username=${username}`, (error, response, html) => {
      if (!error && response.statusCode === 200) {
        parser.parseString(html, (err, result) => {
          
        }

        $('item').each(function (i, elem) {
          let bgArrayItem = {};
          bgArrayItem.id = $(this).attr('objectid');
          bgArrayItem.type = $(this).attr('subtype');
          bgArrayItem.name = $('name')[0].text();
          bgArray.push(bgArrayItem);
        });

        
        // bgArray.minPlayers = $('minplayers').attr('value');
        // bgArray.maxPlayers = $('maxplayers').attr('value');
        // bgArray.minPlayTime = $('minplaytime').attr('value');
        // bgArray.maxPlayTime = $('maxplaytime').attr('value');
        // bgArray.weight = $('averageweight').attr('value');
        resolve(bgArray);
      } else {
        reject('error');
      }
    });
  });
};

getCollectionData(username).then((res) => {
  const collectionData = res;
  console.log(JSON.stringify(collectionData, undefined, 4));
}, (errorMessage) => {
  console.log(errorMessage);
});
