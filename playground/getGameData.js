const request = require('request');
const cheerio = require('cheerio');

const gamesList = [1, 822];
const gameArray = {};

const getGameData = (gameID) => {
  return new Promise((reject, resolve) => {
    request(`https://www.boardgamegeek.com/xmlapi2/thing?id=${gameID}&stats=1`, (error, response, html) => {
      if (!error && response.statusCode === 200) {
        const $ = cheerio.load(html, {
          xmlMode: true,
        });
        gameArray.name = $('name').attr('value');
        gameArray.minPlayers = $('minplayers').attr('value');
        gameArray.maxPlayers = $('maxplayers').attr('value');
        gameArray.minPlayTime = $('minplaytime').attr('value');
        gameArray.maxPlayTime = $('maxplaytime').attr('value');
        gameArray.weight = $('averageweight').attr('value');
        resolve(gameArray);
      } else {
        reject('error');
      }
    });
  });
};

gamesList.forEach((game) => {
  getGameData(game).then((res) => {
    const gameData = res;
    console.log(JSON.stringify(gameData, undefined, 4));
  }, (errorMessage) => {
    console.log(errorMessage);
  });
});
