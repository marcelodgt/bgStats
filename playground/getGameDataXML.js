const xml2js = require('xml2js');
const request = require('request');

const parser = xml2js.Parser();
const gamesList = [1, 822];

const getGameType = (gameID) => {
  if (gameID === 1) {
    return 1;
  }
  return 822;
};

const getGameData = gameID => new Promise((resolve, reject) => {
  request(`https://www.boardgamegeek.com/xmlapi2/thing?id=${gameID}&stats=1`, (error, response, html) => {
    if (!error || response.statusCode === 200) {
      parser.parseString(html, (err, result) => {
        const gameDescription = result.items.item[0].description;
        const gameName = result.items.item[0].name[0].$.value;
        const gameMinPlayers = result.items.item[0].minplayers[0].$.value;
        const gameMaxPlayers = result.items.item[0].maxplayers[0].$.value;
        const gameMinAge = result.items.item[0].minage[0].$.value;
        const gameMinPlaytime = result.items.item[0].minplaytime[0].$.value;
        const gameMaxPlaytime = result.items.item[0].maxplaytime[0].$.value;
        const gameWeight = result.items.item[0].statistics[0].ratings[0].averageweight[0].$.value;
        const gameType = getGameType(gameID);
        console.log(`gameType:${gameType}`);
        const gameObj = {
          gameName,
          gameType,
          gameMinPlayers,
          gameMaxPlayers,
          gameMinPlaytime,
          gameMaxPlaytime,
          gameMinAge,
          gameWeight,
          gameDescription,
        };
        resolve(gameObj);
      });
    } else {
      reject(console.log('Conection error'));
    }
  });
});

gamesList.forEach((game) => {
  getGameData(game).then((res) => {
    const gameData = res;
    console.log(JSON.stringify(gameData, undefined, 4));
  }, (errorMessage) => {
    console.log(errorMessage);
  });
});
