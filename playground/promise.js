// basicExample
// var somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve('it worked.');
//     reject('Something went wrong.')
//   }, 2500);
  
// });

// somePromise.then((message) => {
//   console.log('Sucess: ', message);
// }, (errorMessage) => {
//   console.log('Error: ', errorMessage);
// });



var asyncAdd = (a,b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof a === 'number' || typeof b === 'number') {
        resolve (a + b);
      } else {
        reject('arguments must be numbers')
      }
    },1500)
  });
};

asyncAdd(1,2).then((res) => {
  console.log('result: ', res);
}, (errorMessage) => {
  console.log(errorMessage);
});




