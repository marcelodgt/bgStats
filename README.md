# BgStats (need another name)

## Overview

Esse sistema tenta unificar as funcionalidades de apps já utilizados como bgstas e bggeek em um só.
Além disso, aumentar as estatisticas e funções oferecidas em um pacote completo para que o usuário
não precise recorrer a diversos aplicativos.

O sistema pretende fornecer as seguintes funcionalidades
* Coleção: Gerenciamento da coleção de jogos de tabuleiro do usuário.
* Partidas: Cadastro, edição e visualização das partidas.
* Jogadores: Cadastro, ediçao e estatísticas para cada jogador.
* Grupos: Criação, edição e estatísticas de grupos de jogos criados pelo usuário.
* Usuário: Todo tipo de estatistica para o usuário.
